//
//  main.m
//  UI Customization Examples
//
//  Created by Martin Grider on 1/10/13.
//  Copyright (c) 2013 Abstract Puzzle. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UICE_AppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([UICE_AppDelegate class]));
	}
}
