//
//  UICE_OptionsViewController.m
//  UI Customization Examples
//
//  Created by Martin Grider on 1/17/13.
//  Copyright (c) 2013 Abstract Puzzle. All rights reserved.
//

#import "UICE_OptionsViewController.h"
#import "UICE_ApperanceHelper.h"


@interface UICE_OptionsViewController ()

@end


@implementation UICE_OptionsViewController

@synthesize textField = _textField;
@synthesize slider = _slider;
@synthesize largeIndicator = _largeIndicator;
@synthesize smallIndicator = _smallIndicator;
@synthesize segmentedControl = _segmentedControl;
@synthesize onSwitch = _onSwitch;
@synthesize navigationBar = _navigationBar;


#pragma mark - IBActions

- (IBAction)doneButtonPressed:(id)sender
{
	[self.presentingViewController dismissModalViewControllerAnimated:YES];
}

- (IBAction)colorSegmentValueChanged:(UISegmentedControl *)sender
{
	NSString *colorStr = [sender titleForSegmentAtIndex:[sender selectedSegmentIndex]];
	UIColor *color;
	if ([colorStr isEqualToString:@"Red"]) {
		color = [UIColor redColor];
	}
	else if ([colorStr isEqualToString:@"Green"]) {
		color = [UIColor greenColor];
	}
	else if ([colorStr isEqualToString:@"Blue"]) {
		color = [UIColor blueColor];
	}
	[self setColorTo:color];
}

- (void)setColorTo:(UIColor*)color
{
	[_navigationBar setTintColor:color];
	[_textField setTextColor:color];
	[_slider setMinimumTrackTintColor:color];
	[_largeIndicator setColor:color];
	[_smallIndicator setColor:color];

	[_segmentedControl setTintColor:color];

	// not to be confused with setTintColor: which just changes the round part
	[_onSwitch setOnTintColor:color];

	[UICE_ApperanceHelper setColor:color];
}


#pragma mark - textFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return NO;
}


#pragma mark - boilerplate

- (void)viewDidLoad
{
    [super viewDidLoad];

	// Do any additional setup after loading the view.

	UIColor *color = [[UICE_ApperanceHelper sharedInstance] tintColor];
	[self setColorTo:color];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
	[self setTextField:nil];
	[self setSlider:nil];
	[self setLargeIndicator:nil];
	[self setSmallIndicator:nil];
	[self setSegmentedControl:nil];
	[self setOnSwitch:nil];
	[self setNavigationBar:nil];
	[super viewDidUnload];
}
@end
