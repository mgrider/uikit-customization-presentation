//
//  UICE_OptionsViewController.h
//  UI Customization Examples
//
//  Created by Martin Grider on 1/17/13.
//  Copyright (c) 2013 Abstract Puzzle. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UICE_OptionsViewController : UIViewController <UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *largeIndicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *smallIndicator;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UISwitch *onSwitch;
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;


- (IBAction)doneButtonPressed:(id)sender;
- (IBAction)colorSegmentValueChanged:(UISegmentedControl *)sender;


@end
