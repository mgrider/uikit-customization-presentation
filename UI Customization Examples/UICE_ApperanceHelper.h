//
//  UICE_ApperanceHelper.h
//  UI Customization Examples
//
//  Created by Martin Grider on 1/17/13.
//  Copyright (c) 2013 Abstract Puzzle. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UICE_ApperanceHelper : NSObject


@property (strong, nonatomic) UIColor *tintColor;


+ (void)customizeAppearance;
+ (void)setColor:(UIColor *)color;

+ (UICE_ApperanceHelper *)sharedInstance;


@end
