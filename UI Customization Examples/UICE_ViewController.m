//
//  UICE_ViewController.m
//  UI Customization Examples
//
//  Created by Martin Grider on 1/10/13.
//  Copyright (c) 2013 Abstract Puzzle. All rights reserved.
//

#import "UICE_ViewController.h"


@interface UICE_ViewController ()

@end


@implementation UICE_ViewController

@synthesize textLabel1 = _textLabel1;
@synthesize textLabel2 = _textLabel2;
@synthesize textButton1 = _textButton1;


#pragma mark - boilerplate

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	// ...notably, customize appearance.

	// you can still customize!
	[_textLabel2 setFont:[UIFont fontWithName:@"TartanCabaret" size:50.0f]];
	[_textLabel2 setText:@"AWESOME!"];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTextLabel1:nil];
    [self setTextLabel2:nil];
    [self setTextButton1:nil];
    [super viewDidUnload];
}

@end
