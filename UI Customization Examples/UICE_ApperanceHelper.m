//
//  UICE_ApperanceHelper.m
//  UI Customization Examples
//
//  Created by Martin Grider on 1/17/13.
//  Copyright (c) 2013 Abstract Puzzle. All rights reserved.
//

#import "UICE_ApperanceHelper.h"
#import "UICE_ViewController.h"


@implementation UICE_ApperanceHelper

@synthesize tintColor = _tintColor;


#pragma mark - launching point

+ (void)customizeAppearance
{
	[self customizeText];

	[self customizeNavigationBars];

	[self customizeUIKitForms];
}


#pragma mark - color

+ (void)setColor:(UIColor *)color
{
	[[UICE_ApperanceHelper sharedInstance] setTintColor:color];
	[self customizeAppearance];
}


#pragma mark - setting appearance values

+ (void)customizeUIKitForms
{
	UIColor *tintColor = [[UICE_ApperanceHelper sharedInstance] tintColor];

	NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
									[UIFont boldSystemFontOfSize:14.0f], UITextAttributeFont,
									tintColor, UITextAttributeTextColor,
									nil];
	[[UISegmentedControl appearance] setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
	[[UITextField appearance] setFont:[UIFont systemFontOfSize:14.0f]];
}


+ (void)customizeNavigationBars
{
	UIColor *tintColor = [[UICE_ApperanceHelper sharedInstance] tintColor];

	NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
									[UIFont fontWithName:@"TartanCabaret" size:16.0f], UITextAttributeFont,
									nil];
	[[UINavigationBar appearance] setTitleTextAttributes:textAttributes];
	[[UINavigationBar appearance] setTintColor:tintColor];

	[[UIBarButtonItem appearance] setTitleTextAttributes:textAttributes forState:UIControlStateNormal];

	[[UINavigationBar appearance] setTitleVerticalPositionAdjustment:6.0f forBarMetrics:UIBarMetricsDefault];
}


+ (void)customizeText
{
	UIColor *tintColor = [[UICE_ApperanceHelper sharedInstance] tintColor];

	// all labels
	[[UILabel appearance] setFont:[UIFont fontWithName:@"Times" size:24]];
	[[UILabel appearance] setTextColor:tintColor];

	// just buttons
	[[UILabel appearanceWhenContainedIn:[UIButton class], nil]
	 setFont:[UIFont fontWithName:@"SarcasticRobot" size:30.0f]];
	[[UILabel appearanceWhenContainedIn:[UIButton class], nil]
	 setTextColor:tintColor];

	// labels in a particular view controller
	[[UILabel appearanceWhenContainedIn:[UICE_ViewController class], nil]
	 setFont:[UIFont fontWithName:@"TartanCabaret" size:20]];
	[[UILabel appearanceWhenContainedIn:[UIButton class], [UICE_ViewController class], nil]
	 setFont:[UIFont fontWithName:@"SarcasticRobot" size:20.0f]];
}


#pragma mark - singleton

static BOOL isInitialized_ = NO;

+ (UICE_ApperanceHelper *)sharedInstance
{
	static dispatch_once_t pred = 0;
	static id object = nil;
	dispatch_once(&pred, ^{
		object = [[UICE_ApperanceHelper alloc] init];
		isInitialized_ = YES;
	});
	return object;
}


@end
