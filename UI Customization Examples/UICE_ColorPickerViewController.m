//
//  UICE_ColorPickerViewController.m
//  UI Customization Examples
//
//  Created by Martin Grider on 1/17/13.
//  Copyright (c) 2013 Abstract Puzzle. All rights reserved.
//

#import "UICE_ColorPickerViewController.h"
#import "RSColorPickerView.h"
#import "UICE_ApperanceHelper.h"


@interface UICE_ColorPickerViewController ()

@end


@implementation UICE_ColorPickerViewController

@synthesize colorPickerView = _colorPickerView;


#pragma mark - color picker delegate

-(void)colorPickerDidChangeSelection:(RSColorPickerView *)cp
{
//	NSLog(@"cp color is %@", [cp selectionColor]);
	[self.navigationController.navigationBar setTintColor:[cp selectionColor]];
	[self.view setBackgroundColor:[cp selectionColor]];
	[UICE_ApperanceHelper setColor:[cp selectionColor]];
}


#pragma mark - boilerplate

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

	[_colorPickerView setDelegate:self];
	[_colorPickerView setBrightness:1.0f];
	[_colorPickerView setCropToCircle:NO]; // Defaults to YES (and you can set BG color)
	[_colorPickerView setBackgroundColor:[UIColor whiteColor]];

    // example of preloading a color
//	NSLog(@"redColor is %@", [UIColor redColor]);
//	UIColor color = [UIColor colorWithHue:hue saturation:(saturation/1.5f) brightness:darkBrightness alpha:alpha];
//	[_colorPickerView setSelectionColor:[UIColor colorWithHue:0.9f saturation:0.9f brightness:0.9f alpha:1]];
	[_colorPickerView setSelectionColor:[[UICE_ApperanceHelper sharedInstance] tintColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
	[self setColorPickerView:nil];
	[super viewDidUnload];
}

@end
