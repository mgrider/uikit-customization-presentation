//
//  UICE_ViewController.h
//  UI Customization Examples
//
//  Created by Martin Grider on 1/10/13.
//  Copyright (c) 2013 Abstract Puzzle. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UICE_ViewController : UIViewController


@property (weak, nonatomic) IBOutlet UILabel *textLabel1;
@property (weak, nonatomic) IBOutlet UILabel *textLabel2;
@property (weak, nonatomic) IBOutlet UIButton *textButton1;


@end
