//
//  UICE_AppDelegate.h
//  UI Customization Examples
//
//  Created by Martin Grider on 1/10/13.
//  Copyright (c) 2013 Abstract Puzzle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICE_AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
