//
//  UICE_ColorPickerViewController.h
//  UI Customization Examples
//
//  Created by Martin Grider on 1/17/13.
//  Copyright (c) 2013 Abstract Puzzle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSColorPickerView.h"


@interface UICE_ColorPickerViewController : UIViewController <RSColorPickerViewDelegate>


@property (weak, nonatomic) IBOutlet RSColorPickerView *colorPickerView;


@end
